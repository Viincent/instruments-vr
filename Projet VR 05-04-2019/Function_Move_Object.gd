extends RayCast

export (NodePath) var controller_node = null
export (NodePath) var other_controller_node = null
export (NodePath) var origin_node = null

var menu_node = null

export (Color) var can_move_color = Color(0.0, 1.0, 0.0, 1.0)
export (Color) var cant_move_color = Color(1.0, 0.0, 0.0, 1.0)

var original_collision_layer
var original_collision_mask

var controller = null
var other_controller = null 
var origin = null

var is_object_moving
var object_selected
var let_go
var changing_selected_trigger

var last_position = Vector3(0.0, 0.0, 0.0)
var velocity = Vector3(0.0, 0.0, 0.0)
var previous_transform = null
var current_control = null

func _ready():
	
	controller = get_node(controller_node)
	other_controller = get_node(other_controller_node)
	origin = get_node(origin_node)
	menu_node = get_tree().get_root().get_child(0).get_node("Menu")
func _process(delta):
	if(controller.can_pick):
		velocity = (global_transform.origin - last_position) / delta
		last_position = global_transform.origin
		
		if(controller.is_button_pressed(15)):
			if(is_object_moving):
				let_go = true
			elif(self.is_colliding()):
				$MeshInstance.visible=true
				$MeshInstance.get_surface_material(0).albedo_color = can_move_color
				set_process(true)
				object_selected = self.get_collider()
				
			else:
				$MeshInstance.visible=true
				$MeshInstance.get_surface_material(0).albedo_color = cant_move_color
				object_selected = null
		else:
			if(!is_object_moving):
				$MeshInstance.visible=false
				if(object_selected!=null):
					if(object_selected.has_method('pick_up')):
						is_object_moving = true
						original_collision_mask = self.collision_mask
						object_selected.pick_up(controller)
						previous_transform = object_selected.transform
						controller.can_move = false
						controller.can_teleport = false
						other_controller.can_move = false
						other_controller.can_pick = false
			if(let_go):
				let_go=false
				is_object_moving = false
				object_selected.let_go(velocity)
				object_selected = null
				controller.can_move = true
				controller.can_move = true
				other_controller.can_teleport = true
				other_controller.can_pick = true
			if(is_object_moving):
				var forwards_backwards = controller.get_joystick_axis(1)
				var left_right = controller.get_joystick_axis(0)
				
				var forwards_backwards_other = other_controller.get_joystick_axis(1)
				var left_right_other = other_controller.get_joystick_axis(0)
				
				if (controller.is_button_pressed(14) && abs(forwards_backwards) > 0.4):
					var dir = (origin.get_global_transform().origin - object_selected.get_global_transform().origin).normalized()
					object_selected.global_translate(-dir*delta*forwards_backwards*2)
				if (controller.is_button_pressed(14) && abs(left_right) > 0.4):
					object_selected.scale += Vector3(left_right*delta,left_right*delta,left_right*delta)
					
				if(controller.is_button_pressed(2)):
					object_selected.transform = previous_transform
				#pour changer de slider dans le menu
				#pour changer la couleur
				if(other_controller.is_button_pressed(14) && forwards_backwards_other > 0.4):
					menu_node.value_slider(1, object_selected)
				if(other_controller.is_button_pressed(14) && forwards_backwards_other < -0.4):
					menu_node.value_slider(-1, object_selected)
				if(other_controller.is_button_pressed(15)):
					changing_selected_trigger = true
				if(changing_selected_trigger && !other_controller.is_button_pressed(15)):
					changing_selected_trigger = false
					print (menu_node.name)
					menu_node.change_selected_trigger()
				
				#.translation = Vector3($MeshInstance.global_transform[3][0]-3,$MeshInstance.global_transform[3][1],$MeshInstance.global_transform[3][2])
			
		