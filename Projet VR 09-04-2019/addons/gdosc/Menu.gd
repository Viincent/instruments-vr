extends Node

var object = null

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass


func setupMenu(object_selected):
	object = object_selected
	var meshInstance = object_selected.get_node("MeshInstance")
	var color = meshInstance.get_surface_material(0).albedo_color
	get_node("Viewport/Control/SliderR/Value").value = color[0]*255
	get_node("Viewport/Control/SliderR/TextValue").text = str(int(color[0]*255))
	get_node("Viewport/Control/SliderG/Value").value = color[1]*255
	get_node("Viewport/Control/SliderG/TextValue").text = str(int(color[1]*255))
	get_node("Viewport/Control/SliderB/Value").value = color[2]*255
	get_node("Viewport/Control/SliderB/TextValue").text = str(int(color[2]*255))
	get_node("Viewport/Control/SliderA/Value").value = color[3]*255
	get_node("Viewport/Control/SliderA/TextValue").text = str(color[3])
	
	get_node("Viewport/Control/SliderH/Value").value = meshInstance.scale[0]/5*255
	get_node("Viewport/Control/SliderH/TextValue").text = str(stepify(meshInstance.scale[0],0.01))
	get_node("Viewport/Control/SliderL/Value").value = meshInstance.scale[1]/5*255
	get_node("Viewport/Control/SliderL/TextValue").text = str(stepify(meshInstance.scale[1],0.01))
	get_node("Viewport/Control/SliderP/Value").value = meshInstance.scale[2]/5*255
	get_node("Viewport/Control/SliderP/TextValue").text = str(stepify(meshInstance.scale[2],0.01))
	
	update_menu_pos()

func update_menu_pos():
	var camera = get_tree().get_root().get_child(0).get_node("ARVROrigin/ARVRCamera")
	get_node("Area").look_at_from_position(object.global_transform[3]+4*camera.get_global_transform().basis.x, camera.global_transform[3], Vector3(0,1,0))



func change_selected_trigger():
	if($Viewport/Control.current_index < $Viewport/Control.sliders.size()-1):
		$Viewport/Control.current_index += 1
	else:
		$Viewport/Control.current_index = 0
		
		
func value_slider(nombre, object_selected):
	if($Viewport/Control != null):
		var my_node = $Viewport/Control.current_value
		if(my_node.value < 256 && my_node.value > -1):
			my_node.value += nombre
			var meshInstance = object_selected.get_node("MeshInstance")
			var collision = object_selected.get_node("CollisionShape")
			print (meshInstance.name)
			var color = meshInstance.get_surface_material(0).albedo_color
			var scale = meshInstance.scale
			if ($Viewport/Control.current_index < 3):
				$Viewport/Control.current_text_value.text = str(int(my_node.value))
				match $Viewport/Control.current_index:
					0:
						meshInstance.get_surface_material(0).albedo_color = Color(my_node.value/255.0,color[1],color[2],color[3])
					1:
						meshInstance.get_surface_material(0).albedo_color = Color(color[0],my_node.value/255.0,color[2],color[3])
					2:
						meshInstance.get_surface_material(0).albedo_color = Color(color[0],color[1],my_node.value/255.0,color[3])
			
			elif ($Viewport/Control.current_index ==3) :
				$Viewport/Control.current_text_value.text = str(stepify(my_node.value/255,0.01))
				meshInstance.get_surface_material(0).albedo_color = Color(color[0],color[1],color[2],my_node.value/255)
			
			else:
				$Viewport/Control.current_text_value.text = str(stepify(my_node.value/255*5,0.01))
				match $Viewport/Control.current_index:
					4:
						meshInstance.scale = Vector3(scale[0],my_node.value/255*5,scale[2])
						collision.scale =  Vector3(scale[0],my_node.value/255*5,scale[2])
					5:
						meshInstance.scale = Vector3(scale[0],scale[1],my_node.value/255*5)
						collision.scale =  Vector3(scale[0],scale[1],my_node.value/255*5)
						
					6:
						
						meshInstance.scale = Vector3(my_node.value/255*5,scale[1],scale[2])
						collision.scale = Vector3(my_node.value/255*5,scale[1],scale[2])
						
