extends Spatial
# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var config
var err
var osc_sender
var osc_receiver
var check_coordonnees
var check_distance
var check_hauteur
var check_largeur
var check_profondeur
var check_couleur
var check_autofill
var parameters = [] #array qui contient tous les paramètres x,y,x de la scene (array 2D)
#[[x1,y1,z1,distance,hauteur,largeur,profondeur],[x2,y2,z2,distance,hauteur,largeur,profondeur]...]
var movableObjects
var movableObjectsArray = []

export (NodePath) var movableObjectsNode = null


func _ready():
	movableObjects = get_node(movableObjectsNode)
	for node in movableObjects.get_children():
		movableObjectsArray.push_back(node)
	var adress
	var port_sortie
	var port_entree
	config = ConfigFile.new()
	err = config.load("res://Addons/gdosc/test.cfg")
	osc_sender = load("res://Addons/gdosc/bin/gdoscsender.gdns").new()
	osc_receiver=load("res://Addons/gdosc/bin/gdoscreceiver.gdns").new()
	
	
	if (err == OK):
		adress = config.get_value("osc","adresse")
		port_sortie = config.get_value("osc","port_sortie")
		port_entree = config.get_value("osc","port_entree")
		check_coordonnees = config.get_value("osc","coordonnees")
		check_distance = config.get_value("osc","distance")
		check_hauteur = config.get_value("osc","hauteur")
		check_largeur = config.get_value("osc","largeur")
		check_profondeur = config.get_value("osc","profondeur")
		check_couleur = config.get_value("osc","couleur")
		check_autofill = config.get_value("osc","autofill")
		

		if (adress == null or port_sortie == null or port_entree == null):
			print("Erreur, adresse ou port nil")
			get_tree().quit()
		else:
			osc_sender.setup(adress,port_sortie)
			osc_sender.start()
			osc_receiver.max_queue(20)
			osc_receiver.avoid_duplicate(true)
			osc_receiver.setup(port_entree)
			osc_receiver.start()
	
	
	#initialisation du tableau contenant les paramètres
	for node in movableObjectsArray:
			parameters.push_back([-1,-1,-1,-1,-1,-1,-1,Color(1,1,1,1)])
	
	if (check_autofill == true):
		osc_sender.msg("/gdosc")
		osc_sender.add("/ask")
		osc_sender.send()
		var array = ["coordonnees","distance","hauteur","largeur","profondeur"]
		for i in range(20000):
			print(i)
			if (osc_receiver.has_message()):
				var msg = osc_receiver.get_next()
				if ("/gdosc/answer" in msg["address"]):
					for name in array:
						if (name in msg["address"]):
							config.set_value("osc",name, true)
							print (name)
						else:
							config.set_value("osc",name, false)
							print(false)
				break
		config.save("res://Addons/gdosc/test.cfg")





func _process(delta):
	#Pour chaque node de la scene, on vérifie si l'objet a changé de position, si c'ets le cas et que ce n'est pas la caméra, on envoie les nouvelles coordonnées

	var indice = 0
	for node in movableObjectsArray:
			if (check_coordonnees == true):
				if (parameters[indice][0] != node.global_transform[3][0] 
				or parameters[indice][1] != node.global_transform[3][1] 
				or parameters[indice][2] != node.global_transform[3][2]):
					osc_sender.msg("/gdosc")
					osc_sender.add("/" + node.name + "/position") 
					osc_sender.add(node.global_transform[3])
					osc_sender.send()
					parameters[indice][0] = node.global_transform[3][0]
					parameters[indice][1] = node.global_transform[3][1]
					parameters[indice][2] = node.global_transform[3][2]
					
			if (check_distance == true):
				var distTemp = _calculDistance(node)
				if (abs(parameters[indice][3] - distTemp) > 0.001):
					osc_sender.msg("/gdosc")
					osc_sender.add("/" + node.name + "/distance")
					osc_sender.add( distTemp)
					osc_sender.send()
					parameters[indice][3] = distTemp
					
			if (check_hauteur == true):
				var mesh = node.get_node("MeshInstance").scale[1]
				if (abs(parameters[indice][4] - mesh) > 0.001):
					osc_sender.msg("/gdosc")
					osc_sender.add("/" + node.name + "/hauteur")
					osc_sender.add(mesh)
					osc_sender.send()
					parameters[indice][4] =mesh
					
			if (check_largeur == true):
				var mesh = node.get_node("MeshInstance").scale[2]
				if (abs(parameters[indice][5] - mesh) > 0.001):
					osc_sender.msg("/gdosc")
					osc_sender.add("/" + node.name + "/largeur")
					osc_sender.add(mesh)
					osc_sender.send()
					parameters[indice][5] = mesh
					
			if (check_profondeur == true):
				var mesh = node.get_node("MeshInstance").scale[0]
				if (abs(parameters[indice][6] - mesh ) > 0.001):
					osc_sender.msg("/gdosc")
					osc_sender.add("/" + node.name + "/profondeur")
					osc_sender.add(mesh)
					osc_sender.send()
					parameters[indice][6] = mesh
			
			if (check_couleur == true):
				if (parameters[indice][7] != node.get_node("MeshInstance").get_surface_material(0).albedo_color):
					osc_sender.msg("/gdosc")
					osc_sender.add("/" + node.name + "/couleur")
					osc_sender.add(node.get_node("MeshInstance").get_surface_material(0).albedo_color)
					osc_sender.send()
					parameters[indice][7] = node.get_node("MeshInstance").get_surface_material(0).albedo_color
					
			indice = indice + 1

	"""
	if (osc_receiver.has_message()):
		var msg = osc_receiver.get_next()
		if ("/gdosc" in msg["address"]):
			var args = msg["args"] #args = les arguments passés en paramètres
			for node in movableObjects.get_children():
				if (node.name in msg["address"]):
					node.global_transform = Transform(Vector3(1,0,0),Vector3(0,1,0),Vector3(0,0,1),Vector3(args[0],args[1],args[2]))
			"""	


func _exit_tree():
	osc_sender.stop()
	
	
func _calculDistance(node):
	var result = 0
	var camera = get_node("ARVROrigin")
	result = sqrt(pow(node.global_transform[3][0] - camera.global_transform[3][0],2) 
	+ pow(node.global_transform[3][1] - camera.global_transform[3][1],2) 
	+ pow(node.global_transform[3][2] - camera.global_transform[3][2],2))
	return result