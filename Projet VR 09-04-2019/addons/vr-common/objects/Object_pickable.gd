extends RigidBody


# Remember some state so we can return to it when the user drops the object
onready var original_parent = get_parent()
onready var original_collision_mask = self.collision_mask
onready var original_collision_layer = self.collision_layer

var node = null
# Who picked us up?
var picked_up_by = null

func _ready():
	node = get_tree().get_root().get_child(0).get_node("Menu")
	
	original_collision_layer = self.collision_layer
	original_collision_mask = self.collision_mask

# we are being picked up by...
func pick_up(by):
	if picked_up_by == by:
		return
#
	if picked_up_by:
		let_go()

	# remember who picked us up
	picked_up_by = by
	
	# turn off physics on our pickable object
	mode = RigidBody.MODE_STATIC
	self.collision_layer = 0
	self.collision_mask = 0
	var t = global_transform
	# now reparent it
	original_parent.remove_child(self)
	picked_up_by.add_child(self)
	global_transform = t
	# reset our transform
#	transform = Transform()
#	translate(global_transform.basis.z)
	#AFFICHAGE DU GUI
	if node != null:
		node.setupMenu(self)
		node.get_node("Area").show()

# we are being let go
func let_go(impulse = Vector3(0.0, 0.0, 0.0)):
	
	if picked_up_by:
		# get our current global transform
		var t = global_transform
		
		# reparent it
		picked_up_by.remove_child(self)
		original_parent.add_child(self)
		
		# reposition it and apply impulse
		global_transform = t
		mode = RigidBody.MODE_RIGID
		self.collision_mask = original_collision_mask
		self.collision_layer = original_collision_layer
		apply_impulse(Vector3(0.0, 0.0, 0.0), impulse)
		
		# we are no longer picked up
		picked_up_by = null
		#print(translation)
		
		if node !=null:
			node.get_node("Area").hide()

