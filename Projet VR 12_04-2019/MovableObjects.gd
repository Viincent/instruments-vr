extends Spatial

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var osc_sender
var config
var address
var port
var err

func _ready():
	config = ConfigFile.new()
	err = config.load("res://Addons/gdosc/test.cfg")
	if (err != OK):
		get_tree().quit()
	
	address = config.get_value("osc","adresse")
	print(address)
	port = config.get_value("osc","port_sortie")
	print(port)
		
	osc_sender = load("res://Addons/gdosc/bin/gdoscsender.gdns").new()
	osc_sender.setup(address,port)
	osc_sender.start()

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass




func _on_Sphere_body_entered(body):
	"""
	osc_sender.msg("/gdosc")
	osc_sender.add("/collision")
	osc_sender.add("/Cube/" + body.name)
	osc_sender.send()
	"""
	pass # replace with function body
