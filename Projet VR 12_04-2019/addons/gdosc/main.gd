extends Spatial
# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var config
var err
var osc_sender
var osc_receiver
var check_autofill
#Variables needed for the receiver and parsing the osc messages
var movableObjects
var movableObjectsArray = []

export (NodePath) var movableObjectsNode = null


func _ready():
	movableObjects = get_node(movableObjectsNode)
	for node in movableObjects.get_children():
		movableObjectsArray.push_back(node)
	var adress
	var port_sortie
	var port_entree
	
	#charge le fichier contenant la configuration du projet (en fonction du dock)
	config = ConfigFile.new()
	err = config.load("res://Addons/gdosc/test.cfg")

	#charge la librarie osc pour l'envoie et la réception des données
	osc_sender = load("res://Addons/gdosc/bin/gdoscsender.gdns").new()
	osc_receiver=load("res://Addons/gdosc/bin/gdoscreceiver.gdns").new()
	
	if (err == OK):
		adress = config.get_value("osc","adresse")
		port_sortie = config.get_value("osc","port_sortie")
		port_entree = config.get_value("osc","port_entree")
		check_autofill = config.get_value("osc","autofill")
		
		if (adress == null or port_sortie == null or port_entree == null):
			print("Erreur, adresse ou port nil")
			get_tree().quit()
		else:
			osc_sender.setup(adress,port_sortie)
			osc_sender.start()
			osc_receiver.max_queue(20)
			osc_receiver.avoid_duplicate(true)
			osc_receiver.setup(port_entree)
			osc_receiver.start()
	else :
		get_tree().quit()





func _process(delta):
	#Pour chaque node de la scene, on vérifie si l'objet a changé de position, si c'ets le cas et que ce n'est pas la caméra, on envoie les nouvelles coordonnées
	pass

	"""
	if (osc_receiver.has_message()):
		var msg = osc_receiver.get_next()
		if ("/gdosc" in msg["address"]):
			var args = msg["args"] #args = les arguments passés en paramètres
			for node in movableObjects.get_children():
				if (node.name in msg["address"]):
					node.global_transform = Transform(Vector3(1,0,0),Vector3(0,1,0),Vector3(0,0,1),Vector3(args[0],args[1],args[2]))
			"""	


func _exit_tree():
	osc_sender.stop()
	