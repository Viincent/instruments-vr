extends Control

var current_slider = null

var sliders = []
var other_sliders = []
var current_index = null
var current_text = null
var current_value = null
var current_text_value = null
export (Color) var active = Color(0.0, 1.0, 0.0, 1.0)
export (Color) var inactive = Color(1.0, 0.0, 0.0, 1.0)

func _ready():
	current_index = 0
	for node in get_children():
		sliders.append(node)
		
	
	
			
func _process(delta):
	current_slider = sliders[current_index]
	for node in sliders :
		if node != current_slider:
			get_node(node.name + "/Text").set("custom_colors/font_color", inactive)
	for node in current_slider.get_children():
		if(node.name == "Text"):
			current_text = node
		elif(node.name == "Value"):
			current_value = node
		elif(node.name == "TextValue"):
			current_text_value = node
	current_text.set("custom_colors/font_color", active)
	