tool
extends EditorPlugin



# class member variables go here, for example:
var dock
var config
var err

var node_adress
var node_port_sortie
var node_port_entree
var node_coordonnees
var node_distance
var node_hauteur
var node_largeur
var node_profondeur
var node_couleur

var previous_adress
var current_adress
var previous_port_sortie
var current_port_sortie
var previous_port_entree
var current_port_entree

var previous_coordonnees
var previous_distance
var previous_hauteur
var previous_largeur
var previous_profondeur
var previous_couleur


func _enter_tree():
	previous_adress = ""
	current_adress = ""
	previous_port_sortie = ""
	current_port_sortie = ""
	previous_port_entree = ""
	current_port_entree = ""
	
	previous_coordonnees = null
	previous_distance = null
	previous_largeur = null
	previous_hauteur = null
	previous_profondeur = null
	previous_couleur = null
	# Called when the node is added to the scene for the first time.
	# Initialization here
	dock = preload("res://Addons/gdosc/Dock.tscn").instance()
	add_control_to_dock(DOCK_SLOT_LEFT_UL, dock)

	config = ConfigFile.new()
	err = config.load("res://Addons/gdosc/test.cfg")
	if not err == OK:
		print ("Erreur dans le chargement du fichier cfg")
		get_tree().quit()
	else:
		node_adress = dock.get_node("../Control/Adresse")
		node_port_sortie = dock.get_node("../Control/Port_sortie")
		node_port_entree = dock.get_node("../Control/Port_entree")
		node_coordonnees = dock.get_node("../Control/Coordonnees")
		node_distance = dock.get_node("../Control/Distance")
		node_hauteur = dock.get_node("../Control/Hauteur")
		node_largeur = dock.get_node("../Control/Largeur")
		node_profondeur = dock.get_node("../Control/Profondeur")
		node_couleur = dock.get_node("../Control/Couleur")
		


func _process(delta):
	
	
	
	#Partie sauvegarde de l'adresse IP et du Port dans le fichier test.cfg
	if (err == OK):

		current_adress = node_adress.text
		current_port_sortie = node_port_sortie.text
		current_port_entree = node_port_entree.text
		
		if (current_adress != previous_adress) :
			config.set_value("osc","adresse",current_adress)
			config.save("res://Addons/gdosc/test.cfg")
			previous_adress = current_adress
			
		if (current_port_sortie != previous_port_sortie) :
			config.set_value("osc","port_sortie",current_port_sortie)
			previous_port_sortie = current_port_sortie
			config.save("res://Addons/gdosc/test.cfg")
			
		if (current_port_entree != previous_port_entree) :
			config.set_value("osc","port_entree",current_port_entree)
			previous_port_entree = current_port_entree
			config.save("res://Addons/gdosc/test.cfg")
	
		#Vérification de la checkbox coordonnées
		if (node_coordonnees.is_pressed() and previous_coordonnees != node_coordonnees.is_pressed()):
			config.set_value("osc","coordonnees", true)
			config.save("res://Addons/gdosc/test.cfg")
			previous_coordonnees = true
		elif (node_coordonnees.is_pressed() == false and previous_coordonnees != node_coordonnees.is_pressed()):
			config.set_value("osc","coordonnees",false)
			config.save("res://Addons/gdosc/test.cfg")
			previous_coordonnees = false
			
			
		#Vérification de la checkbox distance
		if (node_distance.is_pressed() and previous_distance != node_distance.is_pressed()):
			config.set_value("osc","distance", true)
			config.save("res://Addons/gdosc/test.cfg")
			previous_distance = true
		elif (node_distance.is_pressed() == false and previous_distance != node_distance.is_pressed()):
			config.set_value("osc","distance",false)
			config.save("res://Addons/gdosc/test.cfg")
			previous_distance = false
			
			
			
		#Vérification de la checkbox Hauteur
		if (node_hauteur.is_pressed() and previous_hauteur != node_hauteur.is_pressed()):
			config.set_value("osc","hauteur", true)
			config.save("res://Addons/gdosc/test.cfg")
			previous_hauteur = true
		elif (node_hauteur.is_pressed() == false and previous_hauteur != node_hauteur.is_pressed()):
			config.set_value("osc","hauteur",false)
			config.save("res://Addons/gdosc/test.cfg")
			previous_hauteur = false			
			
			
		#Vérification de la checkbox Largeur
		if (node_largeur.is_pressed() and previous_largeur != node_largeur.is_pressed()):
			config.set_value("osc","largeur", true)
			config.save("res://Addons/gdosc/test.cfg")
			previous_largeur = true
		elif (node_largeur.is_pressed() == false and previous_largeur != node_largeur.is_pressed()):
			config.set_value("osc","largeur",false)
			config.save("res://Addons/gdosc/test.cfg")
			previous_largeur = false	
			
			
		#Vérification de la checkbox Profondeur
		if (node_profondeur.is_pressed() and previous_profondeur != node_profondeur.is_pressed()):
			config.set_value("osc","profondeur", true)
			config.save("res://Addons/gdosc/test.cfg")
			previous_profondeur = true
		elif (node_profondeur.is_pressed() == false and previous_profondeur != node_profondeur.is_pressed()):
			config.set_value("osc","profondeur",false)
			config.save("res://Addons/gdosc/test.cfg")
			previous_profondeur = false	
			
			
		#Vérification de la checkbox Couleur
		if (node_couleur.is_pressed() and previous_couleur != node_couleur.is_pressed()):
			config.set_value("osc","couleur", true)
			config.save("res://Addons/gdosc/test.cfg")
			previous_couleur = true
		elif (node_couleur.is_pressed() == false and previous_couleur != node_couleur.is_pressed()):
			config.set_value("osc","couleur",false)
			config.save("res://Addons/gdosc/test.cfg")
			previous_couleur = false	
		
			
		
	else :
		print("Erreur dans le chargement du fichier cfg")
		get_tree().quit()
	pass

func _exit_tree():
	remove_control_from_docks(dock) # Remove the dock
	dock.free() # Erase the control from the memory

