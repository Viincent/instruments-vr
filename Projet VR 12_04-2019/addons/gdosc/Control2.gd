extends Control



var current_slider = null

var sliders = []
var other_sliders = []
var current_index = null
var current_text = null
var current_value = null
var current_checkbox = null
export (Color) var active = Color(0.0, 1.0, 0.0, 1.0)
export (Color) var inactive = Color(1.0, 0.0, 0.0, 1.0)
# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var node_sliders = null

func _ready():
	node_sliders = get_node("Sliders")
	current_index = 0
	for node in node_sliders.get_children():
		sliders.append(node)

func _process(delta):
	current_slider = sliders[current_index]
	for node in sliders :
		if node != current_slider:
			get_node("Sliders/" + node.name + "/Label").set("custom_colors/font_color", inactive)
	for node in current_slider.get_children():
		if(node.name == "Label"):
			current_text = node
		elif(node.name == "HSlider"):
			current_value = node
		elif(node.name == "CheckBox"):
			current_checkbox = node
	current_text.set("custom_colors/font_color", active)
