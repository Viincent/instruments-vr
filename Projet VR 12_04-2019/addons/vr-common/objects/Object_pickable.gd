extends RigidBody


# Remember some state so we can return to it when the user drops the object
onready var original_parent = get_parent()
onready var original_collision_mask = self.collision_mask
onready var original_collision_layer = self.collision_layer

var node_menu = null
var node_mesh = null
# Who picked us up?
var picked_up_by = null

#Variables for the osc connection
var config = null
var err = null
var node_main = null
var check_coordonnees = null
var check_distance = null
var check_hauteur = null
var check_largeur = null
var check_profondeur = null
var check_couleur = null
var parameters = [] #array qui contient tous les derniers paramètres envoyés de l'objet
#[x1,y1,z1,distance,hauteur,largeur,profondeur]
var osc_sender = null
var osc_receiver = null

var sliders_values = [] #contient les valeurs des sliders permettant de choisir le paramètre sonore associé au paramètre de l'objet
var checkbox_values = [] #contient une liste de booléens pour chaque paramètre de l'objet
var object_parameters = [] #contient les paramètres de l'objet (utile pour l'affichage dynamique du second menu)
var sound_parameters = [] #contient les paramètres sonores (utile pour l'affichage dynamique du second menu)

var index_volume = null
var index_sliders_values = null


#initilise les tableaux, les nodes nécessaires et la valeur des checkbox par rapports au dock de l'éditeur
func _ready():
	object_parameters = ["Position x","Position y","Position z","Red","Green","Blue","Heigth","Width","Depth","Distance"]
	sliders_values = [0,1,2,3,4,5,6,7,8,9]
	node_menu = get_tree().get_root().get_child(0).get_node("Menu")
	node_main = get_tree().get_root().get_child(0) #Get the main Spatial Node
	node_mesh = self.get_node("MeshInstance")
	
	config = ConfigFile.new()
	err = config.load("res://Addons/gdosc/test.cfg")
	
	if (err ==OK):
		check_coordonnees = config.get_value("osc","coordonnees")
		check_distance = config.get_value("osc","distance")
		check_hauteur = config.get_value("osc","hauteur")
		check_largeur = config.get_value("osc","largeur")
		check_profondeur = config.get_value("osc","profondeur")
		check_couleur = config.get_value("osc","couleur")
		checkbox_values = [check_coordonnees,check_coordonnees,check_coordonnees,check_couleur,check_couleur,check_couleur,check_hauteur,check_largeur,check_profondeur,check_distance]
		
	else:
		get_tree().quit()
	
	#Initialize the array with the parameters
	parameters = [0,0,0,0,0,0,0,0,0,0]
	
	
	original_collision_layer = self.collision_layer
	original_collision_mask = self.collision_mask
	
	
func _process(delta):
	
	#initialise la connexion à purr data et récupère les paramètres sonores (les paramètres sont spécifiques à l'objet)
	if (osc_sender == null || osc_receiver == null):
		osc_sender = node_main.osc_sender
		osc_receiver = node_main.osc_receiver
		if (osc_sender != null && osc_receiver != null):
			#on envoie 2 fois le message pour compenser le bug de purr-data qui n'envoie pas le bon message la premiere fois
			for j in range (2):
				sound_parameters.clear()
				osc_sender.msg("/gdosc")
				osc_sender.add("/" + self.name + "/ask") 
				osc_sender.send()
				for i in range(10000): #temporisation pour la réception du message osc
					if (osc_receiver.has_message()):
						var ind = 0
						var msg = osc_receiver.get_next()
						if ("/gdosc/answer" in msg["address"]):
							for parameter in msg["args"]:
								sound_parameters.push_back(parameter) #récupération des données sonores
								if (parameter == "volume"):
									index_volume = ind
								ind += 1		
							break
							
	#Si on a des paramètres sonores, alors on va pouvoir envoyer des données
	elif (sound_parameters.size() != 0):
		
		
	    #maj de la transparence de l'objet en fonction du volume
		index_sliders_values = 0
		for i in sliders_values:
			if (i == index_volume && checkbox_values[index_sliders_values] == true):
				break
			index_sliders_values += 1
		if (index_sliders_values < 9 && checkbox_values[index_sliders_values] == true):
			self.get_node("MeshInstance").get_surface_material(0).albedo_color.a = (parameters[index_sliders_values] / 127)
		elif (index_sliders_values == 9 && checkbox_values[index_sliders_values] == true):
			self.get_node("MeshInstance").get_surface_material(0).albedo_color.a = 1 - (parameters[index_sliders_values] / 127)
		else:
			self.get_node("MeshInstance").get_surface_material(0).albedo_color.a = 1
		
		
		
		#envoi de la position X
		if (checkbox_values[0] == true):
			if (parameters[0] != node_mesh.global_transform[3][0]  /20 *127):
				osc_sender.msg("/gdosc")
				osc_sender.add("/" + self.name + "/" + sound_parameters[sliders_values[0]]) 
				osc_sender.add(node_mesh.global_transform[3][0] /20 *127)
				osc_sender.send()
				parameters[0] = node_mesh.global_transform[3][0]  /20 *127

		#envoi de la position y
		if (checkbox_values[1] == true):
			if (parameters[1] != node_mesh.global_transform[3][1] /20 *127):
				osc_sender.msg("/gdosc")
				osc_sender.add("/" + self.name + "/" + sound_parameters[sliders_values[1]]) 
				osc_sender.add(node_mesh.global_transform[3][1] /20 *127)
				osc_sender.send()
				parameters[1] = node_mesh.global_transform[3][1] /20 *127
				
		#envoi de la position z
		if (checkbox_values[2] == true):
			if (parameters[2] != node_mesh.global_transform[3][2] /20 *127):
				osc_sender.msg("/gdosc")
				osc_sender.add("/" + self.name + "/" + sound_parameters[sliders_values[2]]) 
				osc_sender.add(node_mesh.global_transform[3][2] /20 *127)
				osc_sender.send()
				parameters[2] = node_mesh.global_transform[3][2] /20 *127
				
		#envoi de la couleur R
		if (checkbox_values[3] == true):
			if (parameters[3] != node_mesh.get_surface_material(0).albedo_color.r * 127):
				osc_sender.msg("/gdosc")
				osc_sender.add("/" + self.name + "/" + sound_parameters[sliders_values[3]])
				osc_sender.add(node_mesh.get_surface_material(0).albedo_color.r * 127)
				osc_sender.send()
				parameters[3] = node_mesh.get_surface_material(0).albedo_color.r * 127
				
		#envoi de la couleur G
		if (checkbox_values[4] == true):
			if (parameters[4] != node_mesh.get_surface_material(0).albedo_color.g * 127):
				osc_sender.msg("/gdosc")
				osc_sender.add("/" + self.name + "/" + sound_parameters[sliders_values[4]])
				osc_sender.add(node_mesh.get_surface_material(0).albedo_color.g * 127)
				osc_sender.send()
				parameters[4] = node_mesh.get_surface_material(0).albedo_color.g * 127
		
		#envoi de la couleur B
		if (checkbox_values[5] == true):
			if (parameters[5] != node_mesh.get_surface_material(0).albedo_color.b * 127):
				osc_sender.msg("/gdosc")
				osc_sender.add("/" + self.name + "/" + sound_parameters[sliders_values[5]])
				osc_sender.add(node_mesh.get_surface_material(0).albedo_color.b * 127)
				osc_sender.send()
				parameters[5] = node_mesh.get_surface_material(0).albedo_color.b * 127
				
		#envoi de la hauteur
		if (checkbox_values[6]  == true):
			var mesh = node_mesh.scale[1] / 5 * 127
			if (abs(parameters[6] - mesh) > 0.001):
				osc_sender.msg("/gdosc")
				osc_sender.add("/" + self.name + "/" + sound_parameters[sliders_values[6]])
				osc_sender.add(mesh)
				osc_sender.send()
				parameters[6] =mesh
				
		#envoi de la largeur
		if (checkbox_values[7]  == true):
			var mesh = node_mesh.scale[2] / 5 * 127
			if (abs(parameters[7] - mesh) > 0.001):
				osc_sender.msg("/gdosc")
				osc_sender.add("/" + self.name + "/" + sound_parameters[sliders_values[7]])
				osc_sender.add(mesh)
				osc_sender.send()
				parameters[7] = mesh
				
		#envoi de la profondeur
		if (checkbox_values[8]  == true):
			var mesh = node_mesh.scale[0] / 5 * 127
			if (abs(parameters[8] - mesh) > 0.001):
				osc_sender.msg("/gdosc")
				osc_sender.add("/" + self.name + "/" + sound_parameters[sliders_values[8]])
				osc_sender.add(mesh)
				osc_sender.send()
				parameters[8] = mesh
				
		#envoi de la distance
		if (checkbox_values[9] == true):
			var distTemp = _calculDistance(self) / 15*127
			if (abs(parameters[9] - distTemp) > 0.001):
				osc_sender.msg("/gdosc")
				osc_sender.add("/" + self.name + "/" + sound_parameters[sliders_values[9]])
				osc_sender.add(distTemp)
				osc_sender.send()
				parameters[9] = distTemp
			
		

# we are being picked up by...
func pick_up(by,other_controller):
	if picked_up_by == by:
		return
#
	if picked_up_by:
		let_go()

	# remember who picked us up
	picked_up_by = by
	
	# turn off physics on our pickable object
	mode = RigidBody.MODE_STATIC
	self.collision_layer = 0
	self.collision_mask = 0
	var t = global_transform
	# now reparent it
	original_parent.remove_child(self)
	picked_up_by.add_child(self)
	global_transform = t
	# reset our transform
#	transform = Transform()
#	translate(global_transform.basis.z)
	#AFFICHAGE DU GUI
	if node_menu != null:
		node_menu.setupMenu(self,other_controller)

# we are being let go
func let_go(impulse = Vector3(0.0, 0.0, 0.0)):
	
	if picked_up_by:
		# get our current global transform
		var t = global_transform
		
		# reparent it
		picked_up_by.remove_child(self)
		original_parent.add_child(self)
		
		# reposition it and apply impulse
		global_transform = t
		mode = RigidBody.MODE_RIGID
		self.collision_mask = original_collision_mask
		self.collision_layer = original_collision_layer
		apply_impulse(Vector3(0.0, 0.0, 0.0), impulse)
		
		# we are no longer picked up
		picked_up_by = null
		#print(translation)
		
		if node_menu !=null:
			node_menu.get_node("Area").hide()
			node_menu.viewport = ""


#calcul la distance entre l'utilisateur et la caméra
func _calculDistance(node):
	var result = 0
	var camera = get_tree().get_root().get_child(0).get_node("ARVROrigin")
	result = sqrt(pow(node.global_transform[3][0] - camera.global_transform[3][0],2) 
	+ pow(node.global_transform[3][1] - camera.global_transform[3][1],2) 
	+ pow(node.global_transform[3][2] - camera.global_transform[3][2],2))
	return result