extends RayCast

export (NodePath) var controller_node = null
export (NodePath) var other_controller_node = null
export (NodePath) var origin_node = null

var menu_node = null
var target_node = null 

export (Color) var can_move_color = Color(0.0, 1.0, 0.0, 1.0)
export (Color) var cant_move_color = Color(1.0, 0.0, 0.0, 1.0)

var original_collision_layer
var original_collision_mask

var controller = null
var other_controller = null 
var origin = null

var is_object_moving 
var object_selected
var let_go 
var let_go2 
var temp 

var changing_selected_menu = null
var changing_selected_slider_up = null
var changing_selected_slider_down = null
var changing_selected_checkbox = null
var toggle_rigidbody

var last_position = Vector3(0.0, 0.0, 0.0)
var velocity = Vector3(0.0, 0.0, 0.0)
var previous_transform = null
var current_control = null

var object_speed = null 

func _ready():
	
	controller = get_node(controller_node)
	other_controller = get_node(other_controller_node)
	origin = get_node(origin_node)
	menu_node = get_tree().get_root().get_child(0).get_node("Menu")
	target_node = get_tree().get_root().get_child(0).get_node("SelectedObject")
	
	object_speed = controller.object_speed
	
func _process(delta):
	# check if controller is allowed to pick_object
	if(controller.can_pick):
		
		if(object_selected):
			# constantly check object speed in case object is let go
			velocity = (object_selected.global_transform.origin - last_position) / (delta*10)
			last_position = object_selected.global_transform.origin
		
		if(controller.is_button_pressed(15)):
			# if button is pressed, show ray which changes color depending on the object
			# red : can't be picked up, green : can be picked up
			# if button is pressed a second time, object is let_go but the user can still access the menu
			# if button is pressed a third time, the object is unselected
			# a circle is shown under the selected object
			if(is_object_moving):
				if(!let_go):
					let_go = true
				if(temp):
					temp=false
					let_go2 = true
			elif(self.is_colliding()):
				other_controller.can_pick = false
				$MeshInstance.visible=true
				$MeshInstance.get_surface_material(0).albedo_color = can_move_color
				set_process(true)
				object_selected = self.get_collider()
				target_node.set_selected_object(object_selected)
				target_node.set_target_color(1)
			else:
				other_controller.can_pick = true
				$MeshInstance.visible=true
				$MeshInstance.get_surface_material(0).albedo_color = cant_move_color
				object_selected = null
				target_node.set_selected_object(object_selected)
		else:
			if(!is_object_moving):
				$MeshInstance.visible=false
				if(object_selected!=null):
					if(object_selected.has_method('pick_up')):
						is_object_moving = true
						original_collision_mask = self.collision_mask
						object_selected.pick_up(controller,other_controller)
						previous_transform = object_selected.transform
						controller.can_move = false
						controller.can_teleport = false
						other_controller.can_teleport = false
						other_controller.can_move = false
						target_node.set_target_color(2)
			if(let_go2):
				controller.can_teleport = true
				controller.can_move = true
				other_controller.can_teleport = true
				other_controller.can_pick = true
				other_controller.can_move = true
				object_selected = null
				let_go=false
				let_go2=false
				is_object_moving = false
				target_node.set_selected_object(object_selected)
				if menu_node !=null:
					menu_node.get_node("Area").hide()
					menu_node.viewport = ""
			elif(let_go):
				object_selected.let_go(velocity)
				temp=true
				target_node.set_target_color(3)
			if(is_object_moving):
				var forwards_backwards = controller.get_joystick_axis(1)
				var left_right = controller.get_joystick_axis(0)
				
				var forwards_backwards_other = other_controller.get_joystick_axis(1)
				var left_right_other = other_controller.get_joystick_axis(0)
				
				if (controller.is_button_pressed(14) && abs(forwards_backwards) > 0.4 && !let_go):
					var dir = (origin.get_global_transform().origin - object_selected.get_global_transform().origin).normalized()
					object_selected.global_translate(-dir*delta*forwards_backwards*object_speed)
					
				if (controller.is_button_pressed(14) && abs(left_right) > 0.4 && !let_go):
					var meshInstance = object_selected.get_node("MeshInstance")
					var collision = object_selected.get_node("CollisionShape")
					
					meshInstance.scale += Vector3(left_right*delta,left_right*delta,left_right*delta)
					collision.scale += Vector3(left_right*delta,left_right*delta,left_right*delta)
					for i in range (3) :
						if(meshInstance.scale[i]<0):
							meshInstance.scale[i]=0
							collision.scale[i]=0
						if(meshInstance.scale[i]>5):
							meshInstance.scale[i]=5
							collision.scale[i]=5
							
					menu_node.updateMenu(object_selected)
				
				elif(controller.is_button_pressed(2) && let_go):
					toggle_rigidbody = true
				elif(toggle_rigidbody and !controller.is_button_pressed(2)):
					toggle_rigidbody = false
					object_selected.toggle_rigidbody()

				#pour changer de slider dans le menu
				#pour changer la couleur
				if (other_controller.is_button_pressed(14) && forwards_backwards_other > 0.4):
					changing_selected_slider_up = true
				
				elif(changing_selected_slider_up && !(other_controller.is_button_pressed(14) && forwards_backwards_other > 0.4)):
					menu_node.change_selected_slider("up",object_selected)
					changing_selected_slider_up = false
				
				elif (other_controller.is_button_pressed(14) && forwards_backwards_other < -0.4):
					changing_selected_slider_down = true
					
				elif(changing_selected_slider_down && !(other_controller.is_button_pressed(14) && forwards_backwards_other < -0.4)):
					menu_node.change_selected_slider("down",object_selected)
					changing_selected_slider_down = false
					
				elif(other_controller.is_button_pressed(14) && left_right_other > 0.4):
					menu_node.value_slider(1, object_selected , self)
					
				elif(other_controller.is_button_pressed(14) && left_right_other < -0.4):
					menu_node.value_slider(-1, object_selected, self)	
					
				elif(other_controller.is_button_pressed(15) && !changing_selected_menu):
					changing_selected_menu = true
				elif(changing_selected_menu && !other_controller.is_button_pressed(15)):
					changing_selected_menu = false
					menu_node.change_selected_menu()
				elif(other_controller.is_button_pressed(2) && !changing_selected_checkbox):
					changing_selected_checkbox = true
				elif(changing_selected_checkbox && ! other_controller.is_button_pressed(2)):
					changing_selected_checkbox = false
					menu_node.change_selected_chexBox(object_selected)
					
				#si on veux que le menu suive l'objet
				menu_node.update_menu_pos()
	else:
		$MeshInstance.visible=false

func reset():	
	
	if(!let_go):
		object_selected.let_go(velocity)
	controller.can_move = true
	controller.can_teleport = true
	other_controller.can_teleport = true
	other_controller.can_pick = true
	other_controller.can_move = true
	object_selected = null
	let_go=false
	let_go2=false
	temp=false
	is_object_moving = false
	target_node.set_selected_object(object_selected)
	if menu_node !=null:
		menu_node.get_node("Area").hide()
		menu_node.viewport = ""