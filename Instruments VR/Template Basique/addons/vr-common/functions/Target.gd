extends MeshInstance

var original_parent = null 
var original_global_transform = null

func stop():
	original_global_transform = global_transform
	original_parent = get_parent()
	original_parent.remove_child(self)
	original_parent.get_parent().get_parent().get_parent().add_child(self)
	global_transform = original_global_transform
func reparent():
	original_parent.get_parent().get_parent().get_parent().remove_child(self)
	original_parent.add_child(self)