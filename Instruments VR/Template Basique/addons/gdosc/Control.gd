extends Control

var current_slider = null

var sliders = [] #contient tous les noeuds des sliders
var current_index = null #indice correspondant à la ligne sur laquelle on se trouve
var current_text = null #noeud correspondant au texte associé à la ligne
var current_value = null #noeud correspond au slider associé à la ligne
var current_text_value = null #noeud correspond au texte associé à la valeur du slider associé à la ligne
var active = Color(0.0, 1.0, 0.0, 1.0)
var inactive = Color(1.0, 0.0, 0.0, 1.0)

func _ready():
	current_index = 0
	for node in get_children():
		sliders.append(node)
		
	
	
			
func _process(delta):
	current_slider = sliders[current_index]
	#on change la couleur des labels non selectionnés
	for node in sliders :
		if node != current_slider:
			if (node.name != "Reset"):
				get_node(node.name + "/Text").set("custom_colors/font_color", inactive)
			else:
				get_node(node.name + "/Button").set("custom_colors/font_color", Color(1.0, 1.0, 1.0, 1.0))
	#on récupère les noeuds associés à la ligne sélectionnée
	for node in current_slider.get_children():
		if(node.name == "Text"):
			current_text = node
		elif(node.name == "Value"):
			current_value = node
		elif(node.name == "TextValue"):
			current_text_value = node
		elif(node.name == "Button"):
			current_text = node
			current_value = node
	#on change la couleur de la ligne sélectionnée
	current_text.set("custom_colors/font_color", active)
	