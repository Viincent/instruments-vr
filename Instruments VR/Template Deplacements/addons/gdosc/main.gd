extends Spatial

#variables liées au fichier de configuration my_config
var config = null
var err = null
#variables pour l'envoi et la réception de messages osc
var osc_sender = null
var osc_receiver = null
var check_dataReception = null
#Variables pour stocker les noeuds des objets
var movableObjects = null
var movableObjectsArray = []

var msg      #message OSC
var args	 #argument du message
var address  #adresse du message

var menu #noeud correspondant au menu

#var pour récupérer le path vers les objets
export (NodePath) var movableObjectsNode = null


func _ready():
	#on récupère tous les objets de la scène présents dans movableobjects
	movableObjects = get_node(movableObjectsNode)
	for node in movableObjects.get_children():
		movableObjectsArray.push_back(node)
		
	var adress
	var port_sortie
	var port_entree
	
	menu = get_node("Menu")
	
	#charge le fichier contenant la configuration du projet (en fonction du dock)
	config = ConfigFile.new()
	err = config.load("res://Addons/gdosc/my_config.cfg")

	#charge la librarie osc pour l'envoie et la réception des données
	osc_sender = load("res://Addons/gdosc/bin/gdoscsender.gdns").new()
	osc_receiver=load("res://Addons/gdosc/bin/gdoscreceiver.gdns").new()
	
	if (err == OK):
		#récupère les valeurs présentent dans le fichier de configuration
		adress = config.get_value("osc","adresse")
		port_sortie = config.get_value("osc","port_sortie")
		port_entree = config.get_value("osc","port_entree")
		check_dataReception = config.get_value("osc","dataReception")
		
		if (adress == null or port_sortie == null or port_entree == null):
			print("Erreur, adresse ou port nil")
			get_tree().quit()
		else:
			#initialisation des variables pour envoyer les messages osc
			osc_sender.setup(adress,port_sortie)
			osc_sender.start()
			osc_receiver.max_queue(20)
			osc_receiver.avoid_duplicate(true)
			osc_receiver.setup(port_entree)
			osc_receiver.start()
	else :
		get_tree().quit()


func _process(delta):
	#On vérifie la réception des message et si la réception est activée dans le dock, on met à jour l'objet et le menu
	if (osc_receiver.has_message() && check_dataReception):
		msg = osc_receiver.get_next()
		address = msg["address"]
		if ("/gdosc" in address):
			args = msg["args"] #args = les arguments passés en paramètres
			for node in movableObjectsArray:
				if (node.name in address): #On vérifie que le nom de l'objet dans le message est présent dans la scène
					address = address.substr(address.rfind("/")+1,address.length())
					match address: #On regarde quel paramètre il faut modifier
						"x":
							node.global_transform[3][0] = args[0]
							menu.updateMenu(node)
						"y":
							node.global_transform[3][1] = args[0]
							menu.updateMenu(node)
						"z":
							node.global_transform[3][2] = args[0]
							menu.updateMenu(node)
						"r":
							node.get_node("MeshInstance").get_surface_material(0).albedo_color.r = args[0]
							menu.updateMenu(node)
						"g":
							node.get_node("MeshInstance").get_surface_material(0).albedo_color.g = args[0]
							menu.updateMenu(node)
						"b":
							node.get_node("MeshInstance").get_surface_material(0).albedo_color.b = args[0]
							menu.updateMenu(node)
						"height":
							node.get_node("MeshInstance").scale[1] =  args[0]
							node.get_node("CollisionShape").scale[1] =  args[0]
							menu.updateMenu(node)
						"length":
							node.get_node("MeshInstance").scale[2] =  args[0]
							node.get_node("CollisionShape").scale[2] =  args[0]
							menu.updateMenu(node)
						"depth":
							node.get_node("MeshInstance").scale[0] =  args[0]
							node.get_node("CollisionShape").scale[0] =  args[0]
							menu.updateMenu(node)




func _exit_tree():
	osc_sender.stop()
	