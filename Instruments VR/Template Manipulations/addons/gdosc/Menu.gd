extends Node

var viewport = null
var temp = null
var controller = null

func _ready():
	viewport = "" #variable contenant le viewport pour la texture affichant le menu
	temp = 0 #variable nécessaire pour gérer la vitesse de variation des sliders

#Met à jour les sliders du premier menu ainsi que les textes associés
func updateMenu(object_selected):
	var meshInstance = object_selected.get_node("MeshInstance")
	#premier menu
	get_node("Viewport1/Control/SliderH/Value").value = meshInstance.scale[1]/5*255
	get_node("Viewport1/Control/SliderH/TextValue").text = str(stepify(meshInstance.scale[1],0.01))
	get_node("Viewport1/Control/SliderL/Value").value = meshInstance.scale[2]/5*255
	get_node("Viewport1/Control/SliderL/TextValue").text = str(stepify(meshInstance.scale[2],0.01))
	get_node("Viewport1/Control/SliderP/Value").value = meshInstance.scale[0]/5*255
	get_node("Viewport1/Control/SliderP/TextValue").text = str(stepify(meshInstance.scale[0],0.01))
	
	var color = meshInstance.get_surface_material(0).albedo_color
	get_node("Viewport1/Control/SliderR/Value").value = color[0]*255
	get_node("Viewport1/Control/SliderR/TextValue").text = str(int(color[0]*255))
	get_node("Viewport1/Control/SliderG/Value").value = color[1]*255
	get_node("Viewport1/Control/SliderG/TextValue").text = str(int(color[1]*255))
	get_node("Viewport1/Control/SliderB/Value").value = color[2]*255
	get_node("Viewport1/Control/SliderB/TextValue").text = str(int(color[2]*255))
	
	get_node("Viewport1/Control/Sound/Value").pressed = object_selected.sound
	
	#second menu
	var indice = 0
	for node in get_node("Viewport2/Control/Sliders").get_children():
		node.get_node("HSlider").value = object_selected.sliders_values[indice]
		node.get_node("CheckBox").pressed = object_selected.checkbox_values[indice][object_selected.sliders_values[indice]]
		indice +=1
		
	#on met à jour la couleur des parametres sonores
	selected_sound_parameter(object_selected)
	
#Met à jour le menu lors de la sélection de l'objet
func setupMenu(object_selected,controller):
	self.controller = controller
	
	#Initialisation des menus
	updateMenu(object_selected)

	temp = object_selected.sliders_values[$Viewport2/Control.current_index]
	
	#affichage des intiales des paramètres au-dessus des sliders
	if (object_selected.sound_parameters.size() > 0):
		var ind = 0
		for node in get_node("Viewport2/Control/parameters").get_children():
			node.text = object_selected.sound_parameters[ind].substr(0,1)
			ind +=1
		
		#initialisation de l'affichage des paramètres de l'objet et du son
		get_node("Viewport2/Control/Parameter1").text = object_selected.object_parameters[$Viewport2/Control.current_index]
		get_node("Viewport2/Control/Parameter3").text = object_selected.sound_parameters[object_selected.sliders_values[$Viewport2/Control.current_index]]
		update_menu_pos()
	else:
		print ("pas de connection au purr-data")
		
	
	
#met à jour la position du menu pour suivre la manette
func update_menu_pos():
	var camera = get_tree().get_root().get_child(0).get_node("ARVROrigin/ARVRCamera")
	var area = get_node("Area")
	area.global_transform = controller.global_transform
	area.look_at(camera.global_transform[3], Vector3(0,1,0))


#modifie la texture du menu
func change_selected_menu():
	if (viewport == ""):
		get_node("Area/MeshInstance").get_surface_material(0).albedo_texture = get_node("Viewport1").get_texture()
		viewport = "Viewport1"
		get_node("Area").show()
	elif(viewport == "Viewport1"):
		get_node("Area/MeshInstance").get_surface_material(0).albedo_texture = get_node("Viewport2").get_texture()
		viewport = "Viewport2"
	else:
		get_node("Area").hide()
		viewport = ""
	
#met à jour les variables qui contiennent les nodes de la ligne sélectionnée
func change_selected_slider(sens,object_selected):
	#Cas du premier menu
	if (viewport == "Viewport1"):
		if (sens == "down"):
			if($Viewport1/Control.current_index < $Viewport1/Control.sliders.size() - 1):	
				$Viewport1/Control.current_index += 1
			else:
				$Viewport1/Control.current_index = 0
		elif (sens == "up"):
			if($Viewport1/Control.current_index > 0):	
				$Viewport1/Control.current_index -= 1
			else:
				$Viewport1/Control.current_index = $Viewport1/Control.sliders.size() - 1
		else :
			return
			
			
	#Cas du second menu		
	if (viewport == "Viewport2"):
		if (sens == "down"):
			if($Viewport2/Control.current_index < $Viewport2/Control.sliders.size() - 1):	
				$Viewport2/Control.current_index += 1
			else:
				$Viewport2/Control.current_index = 0
		elif (sens == "up"):
			if($Viewport2/Control.current_index > 0):	
				$Viewport2/Control.current_index -= 1
			else:
				$Viewport2/Control.current_index = $Viewport2/Control.sliders.size() - 1
		else :
			return
		if (object_selected.sound_parameters.size() > 0):
			get_node("Viewport2/Control/Parameter1").text = object_selected.object_parameters[$Viewport2/Control.current_index]
			get_node("Viewport2/Control/Parameter3").text = object_selected.sound_parameters[object_selected.sliders_values[$Viewport2/Control.current_index]]
		else:
			print ("pas de connection au purr-data")
		temp = object_selected.sliders_values[$Viewport2/Control.current_index]*10
		#on met à jour la couleur des parametres sonores
		selected_sound_parameter(object_selected)
		
#Met à jour les sliders et les valeurs associées
func value_slider(nombre, object_selected,parent):
	if (viewport == "Viewport1"): #Si on se trouve dans le premier menu
		if($Viewport1/Control != null):
			var my_node = $Viewport1/Control.current_value
			#Si on se trouve dans un des sliders
			if (my_node.get_parent().name != "Sound" and my_node.get_parent().name != "Reset"):
				if(my_node.value < 256 && my_node.value > -1):
					my_node.value += nombre
					var meshInstance = object_selected.get_node("MeshInstance")
					var collision = object_selected.get_node("CollisionShape")
					var color = meshInstance.get_surface_material(0).albedo_color
					var scale = meshInstance.scale
					if ($Viewport1/Control.current_index < 3):
						$Viewport1/Control.current_text_value.text = str(int(my_node.value))
						#cas des 3 premiers sliders
						match $Viewport1/Control.current_index:
							0:
								meshInstance.get_surface_material(0).albedo_color = Color(my_node.value/255.0,color[1],color[2],color[3])
							1:
								meshInstance.get_surface_material(0).albedo_color = Color(color[0],my_node.value/255.0,color[2],color[3])
							2:
								meshInstance.get_surface_material(0).albedo_color = Color(color[0],color[1],my_node.value/255.0,color[3])
					else:
						#cas des 3 derniers sliders
						$Viewport1/Control.current_text_value.text = str(stepify(my_node.value/255*5,0.01))
						match $Viewport1/Control.current_index:
							3:
								meshInstance.scale = Vector3(scale[0],my_node.value/255*5,scale[2])
								collision.scale =  Vector3(scale[0],my_node.value/255*5,scale[2])
							4:
								meshInstance.scale = Vector3(scale[0],scale[1],my_node.value/255*5)
								collision.scale =  Vector3(scale[0],scale[1],my_node.value/255*5)
							5:
								meshInstance.scale = Vector3(my_node.value/255*5,scale[1],scale[2])
								collision.scale = Vector3(my_node.value/255*5,scale[1],scale[2])
			
			#cas du bouton sound
			elif (my_node.get_parent().name == "Sound"):
				if ($Viewport1/Control.current_value.pressed and nombre < 0):
					$Viewport1/Control.current_value.pressed = false
					object_selected.toggle_sound()
				elif ( !($Viewport1/Control.current_value.pressed) and nombre > 0):
					$Viewport1/Control.current_value.pressed = true
					object_selected.toggle_sound()
			#cas du bouton reset	
			elif (my_node.get_parent().name == "Reset"): 
				parent.reset()
				object_selected.reset_data()
				updateMenu(object_selected)
				if (get_node("Viewport1/Control/Sound/Value").pressed == false):
					get_node("Viewport1/Control/Sound/Value").pressed = true
					object_selected.toggle_sound()
				
				
	elif (viewport == "Viewport2"): #Si on se trouve dans le second menu
		if($Viewport2/Control != null):
			var slider = $Viewport2/Control.current_value
			if (slider.value < 10 && slider.value > -1):
				temp += nombre
				if (temp < 0):
					temp = 0
				elif (temp > 100):
					temp = 100
				slider.value = int(temp/10)
				object_selected.sliders_values[$Viewport2/Control.current_index] = slider.value
				$Viewport2/Control.current_checkbox.pressed = object_selected.checkbox_values[$Viewport2/Control.current_index][slider.value]
				if (object_selected.sound_parameters.size() > 0):
					get_node("Viewport2/Control/Parameter3").text = object_selected.sound_parameters[object_selected.sliders_values[$Viewport2/Control.current_index]]
				else:
					print ("pas de connection au purr-data")


#Met à jour la checkbox sélectionnée
func change_selected_chexBox(object_selected):
	if (viewport == "Viewport2"):
		$Viewport2/Control.current_checkbox.pressed = ! $Viewport2/Control.current_checkbox.pressed
		object_selected.checkbox_values[$Viewport2/Control.current_index][object_selected.sliders_values[$Viewport2/Control.current_index]] = $Viewport2/Control.current_checkbox.pressed
		if ($Viewport2/Control.current_checkbox.pressed == true):
			object_selected.send_data_on_change($Viewport2/Control.current_index,object_selected.sliders_values[$Viewport2/Control.current_index])
		#on met à jour la couleur des parametres sonores
		selected_sound_parameter(object_selected)

#Met à jour la couleur des paramètres sonores sélectionnées pour la ligne active
func selected_sound_parameter(object_selected):
	for i in range (object_selected.checkbox_values[$Viewport2/Control.current_index].size()):
		if (object_selected.checkbox_values[$Viewport2/Control.current_index][i] == true):
			get_node("Viewport2/Control/parameters").get_child(i).set("custom_colors/font_color", Color(0.0, 1.0, 0.0, 1.0))
		else:
			get_node("Viewport2/Control/parameters").get_child(i).set("custom_colors/font_color", Color(1.0, 0.0, 0.0, 1.0))

