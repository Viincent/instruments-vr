extends MeshInstance

var selected_object = null

export (Color) var object_touched = Color(0.0, 1.0, 0.0, 1.0)
export (Color) var object_moving = Color(1.0, 1.0, 0.0, 1.0)
export (Color) var object_selected = Color(1.0, 0.5, 0.0, 1.0)

func _physics_process(delta):
	
	if (selected_object != null):
		global_transform.origin.x = selected_object.get_global_transform().origin.x
		global_transform.origin.z = selected_object.get_global_transform().origin.z
		scale.x = (selected_object.get_node("MeshInstance").scale.x) * 2
		scale.z = (selected_object.get_node("MeshInstance").scale.z) * 2
		
func set_selected_object(object):
	if(object == null):
		visible = false
		selected_object = null
	else :
		visible = true
		selected_object = object
		
func set_target_color(color):
	if color == 1:
		get_surface_material(0).albedo_color = object_touched
	elif color == 2:
		get_surface_material(0).albedo_color = object_moving
	elif color == 3:
		get_surface_material(0).albedo_color = object_selected