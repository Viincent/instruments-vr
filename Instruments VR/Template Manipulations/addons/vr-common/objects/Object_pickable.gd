extends RigidBody


# Remember some state so we can return to it when the user drops the object
onready var original_parent = get_parent()
onready var original_collision_mask = self.collision_mask
onready var original_collision_layer = self.collision_layer

var node_menu = null
var node_mesh = null
var node_collision = null
# Who picked us up?
var picked_up_by = null

var gravity_off = null

#Variables for the osc connection
var config = null
var err = null
var node_main = null
#chaque array contient 10 booléen pour chaque paramètre sonore
var check_coordonnees_x = []
var check_coordonnees_y = []
var check_coordonnees_z = []
var check_distance = []
var check_hauteur = []
var check_largeur = []
var check_profondeur = []
var check_couleur_r = []
var check_couleur_g = []
var check_couleur_b = []
var check_brutData = null
var parameters = [] #array qui contient tous les derniers paramètres envoyés de l'objet
#[x1,y1,z1,r,g,b,distance,hauteur,largeur,profondeur,distance]
var osc_sender = null
var osc_receiver = null


var sliders_values = [] #contient les valeurs des sliders permettant de choisir le paramètre sonore associé au paramètre de l'objet
var checkbox_values = [] #contient une liste de booléens pour chaque paramètre de l'objet
var object_parameters = [] #contient les paramètres de l'objet (utile pour l'affichage dynamique du second menu)
var sound_parameters = [] #contient les paramètres sonores (utile pour l'affichage dynamique du second menu)
var has_change = [] #variable qui contient un booleen pour chaque paramètre de l'objet, pour savoir s'il y a eu un changement

var initial_data = [] #contient les paramètres de l'objet au lancement de la scène (pour le reset)
var index_volume = null #indice du volume dans le tableau des paramètres sonores
var index_sliders_values = null #indice du paramètres de l'objet lié au volume
var sound = null

var origin_transform #variable pour reset l'emplacement de l'objet
var transp #variable pour la transparence

#initilise les tableaux, les nodes nécessaires et la valeur des checkbox par rapports au dock de l'éditeur
func _ready():
	
	checkbox_values = [check_coordonnees_x,check_coordonnees_y,check_coordonnees_z,check_couleur_r,check_couleur_g,check_couleur_b,check_hauteur,check_largeur,check_profondeur,check_distance]
	object_parameters = ["positionX","positionY","positionZ","colorR","colorG","colorB","heigth","width","depth","distance"]
	sliders_values = [0,1,2,3,4,5,6,7,8,9]
	has_change = [false,false,false,false,false,false,false,false,false,false]
	node_menu = get_tree().get_root().get_child(0).get_node("Menu")
	node_main = get_tree().get_root().get_child(0) #Get the main Spatial Node
	node_mesh = self.get_node("MeshInstance")
	node_collision =self.get_node("CollisionShape")
	origin_transform = global_transform
	initial_data = [node_mesh.get_surface_material(0).albedo_color.r,node_mesh.get_surface_material(0).albedo_color.g,node_mesh.get_surface_material(0).albedo_color.b,node_mesh.scale[1],node_mesh.scale[2],node_mesh.scale[0]]
	sound = true
	config = ConfigFile.new()
	err = config.load("res://Addons/gdosc/my_config.cfg")
	
	if (err ==OK):
		var temp = ["coordonnees","coordonnees","coordonnees","couleur","couleur","couleur","hauteur","largeur","profondeur","distance"]
		for i in range (10):
			for j in range (10):
				if (sliders_values[i] == j):
					checkbox_values[i].append(config.get_value("osc",temp[i]))
				else:
					checkbox_values[i].append(false)
					
	
		check_brutData = config.get_value("osc","brutData")
	else:
		get_tree().quit()
	
	#Initialize the array with the parameters
	parameters = [0,0,0,0,0,0,0,0,0,0]
	
	
	original_collision_layer = self.collision_layer
	original_collision_mask = self.collision_mask
	
	
func _process(delta):
	
	#initialise la connexion à purr data et récupère les paramètres sonores (les paramètres sont spécifiques à l'objet)
	if (osc_sender == null || osc_receiver == null):
		osc_sender = node_main.osc_sender
		osc_receiver = node_main.osc_receiver
		if (osc_sender != null && osc_receiver != null):
			#on envoie 2 fois le message pour compenser le bug de purr-data qui n'envoie pas le bon message la premiere fois
			for j in range (2):
				sound_parameters.clear()
				osc_sender.msg("/gdosc")
				osc_sender.add("/" + self.name) 
				osc_sender.add("/ask") 
				osc_sender.send()
				for i in range(10000): #temporisation pour la réception du message osc
					if (osc_receiver.has_message()):
						var ind = 0
						var msg = osc_receiver.get_next()
						if ("/gdosc/answer" in msg["address"]):
							for parameter in msg["args"]:
								sound_parameters.push_back(parameter) #récupération des données sonores
								if (parameter == "volume"):
									index_volume = ind
								ind += 1		
							break		
	#Si on a des paramètres sonores, alors on va pouvoir envoyer des données
	elif (sound_parameters.size() != 0):
		
		
	    #maj de la transparence de l'objet en fonction du volume
		if (check_brutData == false):
			index_sliders_values = 0
			for i in sliders_values:
				if (i == index_volume && checkbox_values[index_sliders_values][index_volume] == true):
					break
				index_sliders_values += 1
			if (index_sliders_values < 9):
				self.get_node("MeshInstance").get_surface_material(0).albedo_color.a = (parameters[index_sliders_values] / 127)
			elif (index_sliders_values == 9 && checkbox_values[index_sliders_values][index_volume] == true):
				transp = 1 - (parameters[index_sliders_values] / 127)
				if (transp < 0.15):
					transp = 0.15
				elif (transp > 1):
					transp = 1
				self.get_node("MeshInstance").get_surface_material(0).albedo_color.a = transp
			else:
				self.get_node("MeshInstance").get_surface_material(0).albedo_color.a = 1
			
		
		for i in range (10):
			#Pour les 10 paramètres sonores
			#envoi de la position X
			if (checkbox_values[0][i] == true):
				if (parameters[0] != node_mesh.global_transform[3][0]  /20 *127 or has_change[0] == true):
					has_change[0] = true
					osc_sender.msg("/gdosc")
					if (check_brutData == true):
						osc_sender.add("/" + self.name + "/positionX") 
						osc_sender.add(node_mesh.global_transform[3][0])
						osc_sender.send()
					else :
						osc_sender.add("/" + self.name + "/" + sound_parameters[i]) 
						osc_sender.add(node_mesh.global_transform[3][0] /20 *127)
						osc_sender.send()
					parameters[0] = node_mesh.global_transform[3][0]  /20 *127
	
			#envoi de la position y
			if (checkbox_values[1][i] == true):
				if (parameters[1] != node_mesh.global_transform[3][1] /20 *127 or has_change[1] == true):
					has_change[1] = true
					osc_sender.msg("/gdosc")
					if (check_brutData == true):
						osc_sender.add("/" + self.name + "/positionY") 
						osc_sender.add(node_mesh.global_transform[3][1])
						osc_sender.send()
					else :
						osc_sender.add("/" + self.name + "/" + sound_parameters[i]) 
						osc_sender.add(node_mesh.global_transform[3][1] /20 *127)
						osc_sender.send()
					parameters[1] = node_mesh.global_transform[3][1] /20 *127
					
			#envoi de la position z
			if (checkbox_values[2][i] == true):
				if (parameters[2] != node_mesh.global_transform[3][2] /20 *127 or has_change[2] == true):
					has_change[2] = true
					osc_sender.msg("/gdosc")
					if (check_brutData == true):
						osc_sender.add("/" + self.name + "/positionZ") 
						osc_sender.add(node_mesh.global_transform[3][2])
						osc_sender.send()
					else :
						osc_sender.add("/" + self.name + "/" + sound_parameters[i]) 
						osc_sender.add(node_mesh.global_transform[3][2] /20 *127)
						osc_sender.send()
					parameters[2] = node_mesh.global_transform[3][2] /20 *127
					
			#envoi de la couleur R
			if (checkbox_values[3][i] == true):
				if (parameters[3] != node_mesh.get_surface_material(0).albedo_color.r * 127 or has_change[3] == true):
					has_change[3] = true
					osc_sender.msg("/gdosc")
					if (check_brutData == true):
						osc_sender.add("/" + self.name + "/colorR") 
						osc_sender.add(node_mesh.get_surface_material(0).albedo_color.r)
						osc_sender.send()
					else :
						osc_sender.add("/" + self.name + "/" + sound_parameters[i])
						osc_sender.add(node_mesh.get_surface_material(0).albedo_color.r * 127)
						osc_sender.send()
					parameters[3] = node_mesh.get_surface_material(0).albedo_color.r * 127
					
			#envoi de la couleur G
			if (checkbox_values[4][i] == true):
				if (parameters[4] != node_mesh.get_surface_material(0).albedo_color.g * 127 or has_change[4] == true):
					has_change[4] = true
					osc_sender.msg("/gdosc")
					if (check_brutData == true):
						osc_sender.add("/" + self.name + "/colorG") 
						osc_sender.add(node_mesh.get_surface_material(0).albedo_color.g)
						osc_sender.send()
					else :
						osc_sender.add("/" + self.name + "/" + sound_parameters[i])
						osc_sender.add(node_mesh.get_surface_material(0).albedo_color.g * 127)
						osc_sender.send()
					parameters[4] = node_mesh.get_surface_material(0).albedo_color.g * 127
			
			#envoi de la couleur B
			if (checkbox_values[5][i] == true):
				if (parameters[5] != node_mesh.get_surface_material(0).albedo_color.b * 127 or has_change[5] == true):
					has_change[5] = true
					osc_sender.msg("/gdosc")
					if (check_brutData == true):
						osc_sender.add("/" + self.name + "/colorB") 
						osc_sender.add(node_mesh.get_surface_material(0).albedo_color.b)
						osc_sender.send()
					else :
						osc_sender.add("/" + self.name + "/" + sound_parameters[i])
						osc_sender.add(node_mesh.get_surface_material(0).albedo_color.b * 127)
						osc_sender.send()
					parameters[5] = node_mesh.get_surface_material(0).albedo_color.b * 127
					
			#envoi de la hauteur
			if (checkbox_values[6][i]  == true):
				var mesh = node_mesh.scale[1]
				if (abs(parameters[6] - mesh / 5 * 127) > 0.001 or has_change[6] == true):
					has_change[6] = true
					osc_sender.msg("/gdosc")
					if (check_brutData == true):
						osc_sender.add("/" + self.name + "/heigth") 
						osc_sender.add(mesh)
						osc_sender.send()
					else :
						osc_sender.add("/" + self.name + "/" + sound_parameters[i])
						osc_sender.add(mesh / 5 * 127)
						osc_sender.send()
					parameters[6] =mesh / 5 * 127
					
			#envoi de la largeur
			if (checkbox_values[7][i]  == true):
				var mesh = node_mesh.scale[2]
				if (abs(parameters[7] - mesh / 5 * 127) > 0.001 or has_change[7] == true):
					has_change[7] = true
					osc_sender.msg("/gdosc")
					if (check_brutData == true):
						osc_sender.add("/" + self.name + "/width") 
						osc_sender.add(mesh)
						osc_sender.send()
					else :
						osc_sender.add("/" + self.name + "/" + sound_parameters[i])
						osc_sender.add(mesh / 5 * 127)
						osc_sender.send()
					parameters[7] = mesh / 5 * 127
					
			#envoi de la profondeur
			if (checkbox_values[8][i]  == true):
				var mesh = node_mesh.scale[0]
				if (abs(parameters[8] - mesh / 5 * 127) > 0.001 or has_change[8] == true):
					has_change[8] = true
					osc_sender.msg("/gdosc")
					if (check_brutData == true):
						osc_sender.add("/" + self.name + "/depth") 
						osc_sender.add(mesh)
						osc_sender.send()
					else :
						osc_sender.add("/" + self.name + "/" + sound_parameters[i])
						osc_sender.add(mesh / 5 * 127)
						osc_sender.send()
					parameters[8] = mesh / 5 * 127
					
			#envoi de la distance
			if (checkbox_values[9][i] == true):
				var distTemp = _calculDistance(self)
				if (abs(parameters[9] - distTemp / 15*127) > 0.001 or has_change[9] == true):
					has_change[9] = true
					osc_sender.msg("/gdosc")
					if (check_brutData == true):
						osc_sender.add("/" + self.name + "/distance") 
						osc_sender.add(distTemp)
						osc_sender.send()
					else :
						osc_sender.add("/" + self.name + "/" + sound_parameters[i])
						osc_sender.add(distTemp / 15*127)
						osc_sender.send()
					parameters[9] = distTemp / 15*127
		
		#On remet has_change à false quand tous les messages ont été envoyés
		has_change = [false,false,false,false,false,false,false,false,false,false]
		

# we are being picked up by...
func pick_up(by,other_controller):
	
	if picked_up_by == by:
		return
#
	if picked_up_by:
		let_go()
	
	
	
	# remember who picked us up
	picked_up_by = by
	
	# turn off physics on our pickable object
	mode = RigidBody.MODE_STATIC
	self.collision_layer = 0
	self.collision_mask = 0
	var t = global_transform
	# now reparent it
	original_parent.remove_child(self)
	picked_up_by.add_child(self)
	global_transform = t
	# reset our transform
#	transform = Transform()
#	translate(global_transform.basis.z)
	#AFFICHAGE DU GUI
	if node_menu != null:
		node_menu.setupMenu(self,other_controller)

# we are being let go
func let_go(impulse = Vector3(0.0, 0.0, 0.0)):
	
	if picked_up_by:
		# get our current global transform
		var t = global_transform
		
		# reparent it
		picked_up_by.remove_child(self)
		original_parent.add_child(self)
		
		# reposition it and apply impulse
		global_transform = t
		if(!gravity_off):
			mode = RigidBody.MODE_RIGID
		self.collision_mask = original_collision_mask
		self.collision_layer = original_collision_layer
		apply_impulse(Vector3(0.0, 0.0, 0.0), impulse)
		
		# we are no longer picked up
		picked_up_by = null

func toggle_rigidbody():
	if(mode == RigidBody.MODE_RIGID):
		mode = RigidBody.MODE_STATIC
		gravity_off = true
	elif(mode == RigidBody.MODE_STATIC):
		mode = RigidBody.MODE_RIGID
		gravity_off = false
		
#calcul la distance entre l'utilisateur et la caméra
func _calculDistance(node):
	var result = 0
	var camera = get_tree().get_root().get_child(0).get_node("ARVROrigin")
	result = sqrt(pow(node.global_transform[3][0] - camera.global_transform[3][0],2) 
	+ pow(node.global_transform[3][1] - camera.global_transform[3][1],2) 
	+ pow(node.global_transform[3][2] - camera.global_transform[3][2],2))
	return result


#permet d'envoyer le premier message lorsque la checkbox est cochée, en fonction de si l'on veut les données brutes ou mappées
func send_data_on_change(object_parameter, sound_parameter):
	if (sound_parameters.size() > 0):
		osc_sender.msg("/gdosc")
		osc_sender.add("/" + self.name)
		if (check_brutData == false):
			osc_sender.add("/" + sound_parameters[sound_parameter])
		else:
			osc_sender.add("/" + object_parameters[object_parameter])
		match object_parameter:
			0:
				if (check_brutData == true):
					osc_sender.add(node_mesh.global_transform[3][0])
				else:
					osc_sender.add(node_mesh.global_transform[3][0] /20 *127)
			1:
				if (check_brutData == true):
					osc_sender.add(node_mesh.global_transform[3][1])
				else:
					osc_sender.add(node_mesh.global_transform[3][1] /20 *127)
			2:
				if (check_brutData == true):
					osc_sender.add(node_mesh.global_transform[3][2])
				else:
					osc_sender.add(node_mesh.global_transform[3][2] /20 *127)
			3:
				if (check_brutData == true):
					osc_sender.add(node_mesh.get_surface_material(0).albedo_color.r)
				else:
					osc_sender.add(node_mesh.get_surface_material(0).albedo_color.r * 127)
			4:
				if (check_brutData == true):
					osc_sender.add(node_mesh.get_surface_material(0).albedo_color.g)
				else:
					osc_sender.add(node_mesh.get_surface_material(0).albedo_color.g * 127)
			5:
				if (check_brutData == true):
					osc_sender.add(node_mesh.get_surface_material(0).albedo_color.b)
				else:
					osc_sender.add(node_mesh.get_surface_material(0).albedo_color.b * 127)
			6:
				if (check_brutData == true):
					osc_sender.add(node_mesh.scale[1])
				else:
					osc_sender.add(node_mesh.scale[1] / 5 * 127)
			7:
				if (check_brutData == true):
					osc_sender.add(node_mesh.scale[1])
				else:
					osc_sender.add(node_mesh.scale[1] / 5 * 127)
			8:
				if (check_brutData == true):
					osc_sender.add(node_mesh.scale[1])
				else:
					osc_sender.add(node_mesh.scale[1] / 5 * 127)
			9:
				if (check_brutData == true):
					osc_sender.add(_calculDistance(self))
				else:
					osc_sender.add(_calculDistance(self) / 15*127)
		osc_sender.send()


#Envoie d'un message osc pour activer ou désactiver le son d'un objet
func toggle_sound():
	osc_sender.msg("/gdosc")
	osc_sender.add("/" + self.name)
	osc_sender.add("/togglesound")
	osc_sender.send()
	sound = ! sound
	
#Réinitialise les paramètres de l'objet et les deux fenêtres du menu
func reset_data():
	
	#paramètres de l'objet
	node_mesh.get_surface_material(0).albedo_color.r = initial_data[0]
	node_mesh.get_surface_material(0).albedo_color.g = initial_data[1]
	node_mesh.get_surface_material(0).albedo_color.b = initial_data[2]
	node_mesh.scale[1] = initial_data[3]
	node_collision.scale[1] = initial_data[3]
	node_mesh.scale[2] = initial_data[4]
	node_collision.scale[2] = initial_data[4]
	node_mesh.scale[0] = initial_data[5]
	node_collision.scale[0] = initial_data[5]

	global_transform = origin_transform
	mode = RigidBody.MODE_RIGID
	#réinitialise le tableau pour le second menu
	if (err == OK):
		sliders_values = [0,1,2,3,4,5,6,7,8,9]
		var temp = ["coordonnees","coordonnees","coordonnees","couleur","couleur","couleur","hauteur","largeur","profondeur","distance"]
		for i in range (10):
			for j in range (10):
				if (sliders_values[i] == j):
					checkbox_values[i][j] = (config.get_value("osc",temp[i]))
				else:
					checkbox_values[i][j] = false
	