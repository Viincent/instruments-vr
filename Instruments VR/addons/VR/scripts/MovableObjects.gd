extends Spatial

var osc_sender
var config
var address
var port
var err

func _ready():
	config = ConfigFile.new()
	err = config.load("res://Addons/gdosc/my_config.cfg")
	if (err != OK):
		print("error loading config file")
		get_tree().quit()
	
	address = config.get_value("osc","adresse")
	print(address)
	port = config.get_value("osc","port_sortie")
	print(port)
		
	osc_sender = load("res://Addons/gdosc/bin/gdoscsender.gdns").new()
	osc_sender.setup(address,port)
	osc_sender.start()
	



#func _on_object_name_body_entered(body):
#	osc_sender.msg("/gdosc")
#	osc_sender.add("/object_name")
#	osc_sender.add("/collision")
#	osc_sender.send()

